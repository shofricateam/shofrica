<?php
namespace WeSupply\Toolbox\Model\Config\Source;

class EstimationFormat implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        return [
            ['value' => 'm/d', 'label' => __('04/28')],
            ['value' => 'd/m', 'label' => __('28/04')],
            ['value' => 'F d', 'label' => __('April 28')],
            ['value' => 'd F', 'label' => __('28 April')],
            ['value' => 'd/m/Y', 'label' => __('28/04/2018')],
        ];
    }
}
<?php
/**
 * @category    WeltPixel
 * @package     WeltPixel_SocialLogin
 * @copyright   Copyright (c) 2018 WeltPixel
 */

namespace WeltPixel\SocialLogin\Model\ResourceModel\Sociallogin;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package WeltPixel\SocialLogin\Model\ResourceModel\Sociallogin
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('WeltPixel\SocialLogin\Model\Sociallogin', 'WeltPixel\SocialLogin\Model\ResourceModel\Sociallogin');
    }
}